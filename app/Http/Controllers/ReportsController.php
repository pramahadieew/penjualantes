<?php

namespace App\Http\Controllers;

use App\Product_Masuk;
use App\Product_Keluar;
use PDF;
// use Request;
use Illuminate\Http\Request;
use Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ReportsController extends Controller
{
    private $fields = [ 'store', 'income', 'spending', 'date', 'sum', 'customer_name'];
    public $link = 'reports';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return view($this->link.'.'.'index');
    }

    public function indexout()
    {
        return view($this->link.'.'.'indexOut');
    }

    public function searchout(Request $request) {
        $monthNow = Carbon::now();
        $monthName = $monthNow->format('F');
        $start = $request->get('fromDate');
        $end = $request->get('toDate');
      
        $data = DB::table('product_keluar')
        ->join('products', 'product_keluar.product_id', '=', 'products.id')
        ->join('customers', 'product_keluar.customer_id', '=', 'customers.id')
        ->select('product_keluar.*', 'products.*', 'customers.email')
        ->whereBetween('tanggal', [$start, $end])
        ->get();
        // dd($datas);
        // $summary = $data->sum('sum');
        // $fixSum = $summary * 50 / 100;

        $pdf = PDF::loadView($this->link.'/'.'pdfresout', compact('data', 'monthName', 'start', 'end'));
        return $pdf->stream('report-'.$start .'-'. $end.'.pdf');
    }
}
