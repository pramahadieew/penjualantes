<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['nama','standart_pack','part_number','qty', 'image'];

    protected $hidden = ['created_at','updated_at'];
}
