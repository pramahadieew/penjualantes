@extends('layouts.master')


@section('content')
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Reports</h3>

        <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
            <i class="fa fa-minus"></i></button>
        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
    </div>
    <div class="box-body">
    <form action="{{ route('search') }}" target="_blank" method="POST" enctype="multipart/form-data">
    	@csrf
         <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="form-group">
                <strong>From Date</strong>
                    <input type="date" value="06/05/2021" id="fromDate" name="fromDate" class="form-control input-datepicker">
                </div>
		    </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="form-group">
                <strong>To Date</strong>
                    <input type="date" value="06/05/2021" id="toDate" name="toDate" class="form-control input-datepicker">
                </div>
		    </div>

            <!-- <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="form-group">
                <strong>Target:</strong>
                    <select name="target" data-style="btn-outline-light" title="Pilih Target" class=" form-control selectpicker">
                    <option value="Dropshipper">In Stock</option>
                    <option value="Dropshipper">Out Stock</option>
                    </select>
                </div>
            </div> -->
		    <div class="col-xs-12 col-sm-12 col-md-12">
		        <button type="submit" class="btn btn-sm btn-primary">Get Report</button>
		    </div>
		</div>
    </form>
    </div>
    <!-- /.box-body -->
</div>
@endsection

