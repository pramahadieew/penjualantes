<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <title>Matel Report</title>
</head>
<style>
    .container {
        width: 100%
    }
    .header {
        /* position: relative;
        top: 0; */
        /* top:0; */
        width: 100%;
        height: 100px;
        background-color: #f0f2f2;
        margin: 0;
        padding-left: 10px;
        border-radius: 5px;
        font-family: Arial, Helvetica, sans-serif;
    }

    p {
        margin-top: -20px;
    }

    #drops {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 98%;
        margin-left: 13px;
        margin-top: -40px;
        border: none;
    }

    #drops td, #drops th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #drops tr:nth-child(even){background-color: #f2f2f2;}

    #drops tr:hover {background-color: #ddd;}

    #drops th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: gray;
        color: white;
    }
    h2 {
        float: right;
        margin-top: -40px;
        font-size: 15px;
        margin-right: 10px;
        color: black;
    }
</style>
<body>
    <div class="container">
        <div class="header">
        <h1 style="padding-top: 10px"><img src="https://inventory.lets-mitos.com/mattel-removebg-preview.png" alt="" height="55" width="200"></h1>
            <!-- <p>Muda InTelektual Operating System</p> -->
            <h2>From: {{$start}} To: {{$end}}</h2>
        </div>
        <table id="drops" border='none'>
        <tr>
            <th>Supplier Name</th>
            <th>Part Number</th>
            <th>Product Name</th>
            <th>Qty</th>
            <th>Date</th>
        </tr>
    @foreach($data as $item)
        <tr>
            <td>{{$item->proses}}</td>
            <td>{{$item->part_number}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->qty}}</td>
            <td>{{$item->tanggal}}</td>
        </tr>
    @endforeach
    </table>
           
    <br/>
    <br/>
    <table style="padding-right: 55px" border="0" width="100%">
        <tr align="right">
            <td style="margin: 200px;">Hormat Kami</td>
        </tr>
    </table>
    <table border="0" width="100%">
    <tr align="right" width="100%">
        <td><img src="https://inventory.lets-mitos.com/ttd-removebg-preview.png" alt="" height="100" width="300"></td>
    </tr>
    </table>
    <table style="padding-right: 50px" border="0" width="100%">
        <tr align="right">
            <td>Wulan Kurniasih</td>
        </tr>
    </table>
    </div>
  <div>
  </div>
</body>
</body>
</html>
